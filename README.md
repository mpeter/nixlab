# Deploy your own GitLab

## Why?

You want a self-hosted fast and safe development workflow with Git, Issue tracking, Continuous Integration and Deployment. You don't want to spend hours setting it up.


## What?

Deploy GitLab CE and GitLab Runner on NixOS on DigitalOcean Droplets (or similar) provisioned with Terraform.


## How?

TL;DR Get a DO token and run `terraform apply`.


### Setup

Register a DigitalOcean account and [generate an access token](https://www.digitalocean.com/community/tutorials/how-to-use-the-digitalocean-api-v2).

Install [Nix](https://nixos.org/nix/) and clone this repo.

Enter a shell environment with all dependencies present, and run the rest of the commands there:

```
nix-shell
```


### Provision and Deploy

Review the infrastructure plan:

```
terraform plan
```

Terraform will ask you for a DigitalOcean security token and an existing DigitalOcean domain. You can [save these variables](https://www.terraform.io/intro/getting-started/variables.html#assigning-variables) so it won't ask again. Do not commit them into the repo.

Provision the infrastructure on DigitalOcean (costs ~$45/mo) and deploy the NIxOS configuration:

```
terraform apply
```

It will take about 10 minutes. When it's done, it will output the IP, domain name and URL of the server.
Visit the URL. If you get 502, wait a minute and try again. Log in with `root` and `CHANGEMENOW`. Change the password.


### CI Runner Registration

Visit `https://your-nixlab-domain/admin/runners` and copy the registration token. Register the runner:

```
./ssh nixlab-register-runner TOKEN
```

If you refresh the page, you should see the new runner.


### Done!

Configure GitLab to your liking. Create projects, add users, use CI.


### Goodies

- all state is on separate `/data` volume
- host `/nix/store` is bind-mounted into CI runners, giving perfect caching for CI jobs using Nix
- straightforward to move to other Terraform [supported cloud providers](https://www.terraform.io/docs/providers/)


## Explore

[`./infrastructure.tf` ](infrastructure.tf) is the Terraform configuration that creates a DigitalOcean SSH key, Volume, Floating IP, Droplet, Domain Record. It's straightforward to port to other .

[`./nixlab.nix`](nixlab.nix) is the NixOS module defining the operating system configuration for the server, including GitLab CE service and GitLab Runner service.

[`./shell.nix`](shell.nix) is the environment loaded by `nix-shell` and contains deployment tools like `terraform`, `ssh`, `rsync`.
