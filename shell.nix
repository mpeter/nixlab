# nix-shell environment for provisioning infrastructure and deploymen

{pkgs ? import ./pinned-nixpkgs.nix {}, ...} :
pkgs.stdenv.mkDerivation {
  name = "nixlab-deploy-shell";
  buildInputs = [ pkgs.terraform pkgs.git pkgs.rsync ];
}
