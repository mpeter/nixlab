# Infrastructure definition

variable "do_token" {
  description = "Digital Ocean access token. See https://www.digitalocean.com/community/tutorials/how-to-use-the-digitalocean-api-v2#how-to-generate-a-personal-access-token"
}

variable "env" {
  description = "Name for the infrastructure environment. It will appear in DigitalOcean resource names and domain names."
  default     = "nixlab"
}

variable "domain" {
  description = "The name of an existing Domain in your DigitalOcean account. Set it up here: https://cloud.digitalocean.com/networking/domains/"
}

variable "do_region" {
  default = "nyc3"
}

provider "digitalocean" {
  token = "${var.do_token}"
}

resource "tls_private_key" "nixlab" {
  algorithm = "RSA"
}

resource "local_file" "private_key" {
  filename = "${path.module}/private_key"
  content  = "${tls_private_key.nixlab.private_key_pem}"

  provisioner "local-exec" {
    command = "chmod 600 ${path.module}/private_key"
  }
}

resource "digitalocean_ssh_key" "nixlab" {
  name       = "nixlab-#{var.env}"
  public_key = "${tls_private_key.nixlab.public_key_openssh}"
}

resource "digitalocean_volume" "nixlab-data" {
  region      = "${var.do_region}"
  name        = "nixlab-data-${var.env}"
  size        = 50
  description = "nixlab-data-${var.env}"

  lifecycle {
    # Comment this out if you are sure you want to DESTROY YOUR DATA
    prevent_destroy = true
  }
}

resource "random_id" "gitlab-secrets-dbpassword" {
  byte_length = 16
}

resource "random_id" "gitlab-secrets-secret" {
  byte_length = 64
}

resource "random_id" "gitlab-secrets-otp" {
  byte_length = 64
}

resource "random_id" "gitlab-secrets-db" {
  byte_length = 64
}

resource "tls_private_key" "gitlab-secrets-jws" {
  algorithm = "RSA"
}

data "template_file" "host-nixos-config" {
  template = <<EOF
# NixOS module for nixlab host system
# Provisioned by Terraform

let
  tablelabsModulePath = "/root/nixlab.nix";
in
  { config, pkgs, lib, ... } :
    if lib.pathExists tablelabsModulePath then
      {
        imports = [ tablelabsModulePath ];

        services.nixlab.domain = "${var.env}.${var.domain}";

        services.gitlab = {
          databasePassword = "${random_id.gitlab-secrets-dbpassword.b64_url}";
          secrets = {
            db = "${random_id.gitlab-secrets-db.b64_url}";
            secret = "${random_id.gitlab-secrets-secret.b64_url}";
            otp = "${random_id.gitlab-secrets-otp.b64_url}";
            jws = ''
${tls_private_key.gitlab-secrets-jws.private_key_pem}
            '';
          };
        };
        fileSystems."/data" = {
          device = "/dev/sda";
          fsType = "ext4";
          autoFormat = true;
          autoResize = true;
        };

        system.autoUpgrade.enable = true;
      }
    else
      lib.warn ("nixlab module not found at " + tablelabsModulePath + ". Skipping nixlab Configuration.") {}
EOF
}

resource "digitalocean_droplet" "nixlab" {
  name       = "nixlab-${var.env}"
  size       = "4gb"
  image      = "debian-9-x64"
  region     = "${var.do_region}"
  ssh_keys   = ["${digitalocean_ssh_key.nixlab.id}"]
  volume_ids = ["${digitalocean_volume.nixlab-data.id}"]
  ipv6       = true

  private_networking = true

  connection {
    user        = "root"
    host        = "${digitalocean_droplet.nixlab.ipv4_address}"
    private_key = "${tls_private_key.nixlab.private_key_pem}"
  }

  provisioner "file" {
    content     = "${data.template_file.host-nixos-config.rendered}"
    destination = "/root/host.nix"
  }

  provisioner "remote-exec" {
    inline = [
      "echo -e 'silent\nshow-error\nretry=2' | tee ~/.curlrc > /dev/null",
      "wget 'https://raw.githubusercontent.com/elitak/nixos-infect/769dce60c939add8fae5bc9f03fc96d8661a76a7/nixos-infect' -O /root/nixos-infect",
      "echo 'Installing NixOS. Logging to /tmp/infect.log.'",
      "NIXOS_IMPORT=/root/host.nix NIX_CHANNEL=nixos-17.09 bash /root/nixos-infect 2>&1 > /tmp/infect.log",
    ]
  }

  provisioner "remote-exec" {
    inline = [
      "nixos-version",
      "cp /old-root/root/host.nix /root/host.nix",
      "cp /old-root/root/.curlrc /root/.curlrc",
      "rm -rf /old-root",
    ]
  }
}

resource "digitalocean_record" "nixlab" {
  domain = "${var.domain}"
  type   = "A"
  name   = "${var.env}."
  value  = "${digitalocean_droplet.nixlab.ipv4_address}"
  ttl    = "300"
}

resource "null_resource" "deploy" {
  depends_on = [
    "digitalocean_record.nixlab",
    "digitalocean_volume.nixlab-data",
  ]

  triggers {
    droplet = "${digitalocean_droplet.nixlab.id}"
    always  = "${uuid()}"
  }

  connection {
    user        = "root"
    host        = "${digitalocean_droplet.nixlab.ipv4_address}"
    private_key = "${tls_private_key.nixlab.private_key_pem}"
  }

  provisioner "remote-exec" {
    inline = ["echo 'Hello, World!'"]
  }

  provisioner "file" {
    content     = "${data.template_file.host-nixos-config.rendered}"
    destination = "/root/host.nix"
  }

  provisioner "file" {
    source      = "${path.module}/nixlab.nix"
    destination = "/root/nixlab.nix"
  }

  provisioner "file" {
    source      = "${path.module}/pinned-nixpkgs.nix"
    destination = "/root/pinned-nixpkgs.nix"
  }

  provisioner "remote-exec" {
    inline = [
      "mkdir -p /data",
      "chmod a+rx /data",
      "true",

      # "nix-channel --remove nixos",
      # "nix-channel --add https://nixos.org/channels/nixos-17.09 nixos",
      # "nix-channel --update",
      #
      # # pin nixpkgs
      # # uncomment the above three and comment this to follow the release channel
      "export NIX_PATH=nixpkgs=https://github.com/NixOS/nixpkgs-channels/archive/d1c8c04ea6b2f09b3062301d1a91f6e5d9af7fb6.tar.gz:nixos=https://github.com/NixOS/nixpkgs-channels/archive/d1c8c04ea6b2f09b3062301d1a91f6e5d9af7fb6.tar.gz:nixos-config=/etc/nixos/configuration.nix",

      "echo -e 'silent\nshow-error\nretry=2' | tee ~/.curlrc > /dev/null",
      "nixos-rebuild switch && sleep 60",
    ]
  }
}

output "ip" {
  value = "${digitalocean_droplet.nixlab.ipv4_address}"
}

output "fqdn" {
  value = "${digitalocean_record.nixlab.fqdn}"
}

output "gitlab_url" {
  value = "https://${digitalocean_record.nixlab.fqdn}/"
}

output "private_key_file" {
  value = "${path.module}/private_key"
}
