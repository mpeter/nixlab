# NixOS module defining nixlab server

{ config, lib, pkgs, ... } :
let
  cfg = config.services.nixlab;
  gitlab-runner-config = ''
    concurrent = 32
    check_interval = 3
  '';
  nixlab-register-runner = pkgs.writeScriptBin "nixlab-register-runner" ''
    #!${pkgs.stdenv.shell}
    if [ "$#" -ne 1 ]; then
      echo "Usage: $0 TOKEN" >&2
      exit 1
    fi
    echo '${gitlab-runner-config}' > /root/gitlab-runnner.conf
    gitlab-runner register \
      --non-interactive \
      --registration-token $1 \
      --url "https://${cfg.domain}/ci" \
      --config /root/gitlab-runner.conf \
      --executor docker \
      --docker-image nixos/nix \
      --docker-volumes "/nix:/nix:ro" \
      --docker-volumes "/nix/var/nix/daemon-socket/socket:/nix/var/nix/daemon-socket/socket" \
      --docker-volumes "/run/current-system/sw:/run/current-system/sw:ro" \
      --docker-volumes "/etc/ssl/certs/ca-certificates.crt:/etc/ssl/certs/ca-certificates.crt:ro" \
      --env "NIX_REMOTE=daemon" \
      --env "NIX_SSL_CERT_FILE=/etc/ssl/certs/ca-certificates.crt" \
      --env "NIX_PATH=nixpkgs=/nix/var/nix/profiles/per-user/root/channels/nixos"
    systemctl restart gitlab-runner
  '';
in {
  options.services.nixlab = {
    domain = lib.mkOption {
      type = lib.types.string;
    };
    debug = lib.mkOption {
      type = lib.types.bool;
      default = false;
    };
  };

  config = {
    networking.extraHosts = "127.0.0.1 ${cfg.domain}";
    networking.firewall.allowedTCPPorts = [ 22 80 443 ];

    environment.systemPackages = [ pkgs.emacs nixlab-register-runner ];

    virtualisation.docker = {
      enable = true;
      # extraOptions = "--data-root /data/docker";
    };

    services.nginx = {
      enable = true;
      recommendedGzipSettings = true;
      recommendedOptimisation = true;
      recommendedProxySettings = true;
      recommendedTlsSettings = true;
      virtualHosts."${cfg.domain}" = {
        enableACME = ! cfg.debug;
        forceSSL = ! cfg.debug;
        locations."/".proxyPass = "http://unix:/run/gitlab/gitlab-workhorse.socket";
      };
    };

    services.gitlab = {
      enable = true;
      initialRootPassword = "CHANGEMENOW";
      host = cfg.domain;
      https = ! cfg.debug;
      port = 443;
      user = "git";
      group = "git";
      statePath = "/data/gitlab-state";
      backupPath = "/data/gitlab-state-backup";
    };

    services.gitlab-runner = {
      enable = true;
      configFile = "/root/gitlab-runner.conf";
      workDir = "/data/gitlab-runner";
    };
    systemd.services.gitlab-runner = {
        preStart = ''
            mkdir -p /data/gitlab-runner
            chown gitlab-runner.gitlab-runner /data/gitlab-runner
          '';
        serviceConfig.PermissionsStartOnly = true;
      };

    services.redis.dbpath = "/data/redis";

    services.postgresql.dataDir = "/data/postgresql";

    # services.postgresqlBackup = {
    #   enable = true;
    #   databases = [ "gitlab" ];
    #   location = "/data/postgresql-backup";
    # };
  };
}
